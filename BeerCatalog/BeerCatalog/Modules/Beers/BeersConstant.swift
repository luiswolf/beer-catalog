//
//  BeersConstant.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

struct BeersConstant {
    
    enum Endpoint {
        static let beers = "https://api.punkapi.com/v2/beers?per_page={pageSize}&page={pageNumber}"
    }
    
    enum Image {
        static let `default` = "beer"
        static let defaultImageUrl = "https://images.punkapi.com/v2/keg.png"
        static let disclosureIndicator = "disclosure-indicator"
    }
    
    enum Label {
        static let abv = "Teor alcoólico"
        static let abvWithValue = "%@ vol."
        static let beers = "Catálogo de cervejas"
        static let ibu = "Escala de amargor"
        static let ibuWithValue = "%@ IBU"
    }
    
    enum Message {
        static let defaultError = "Não foi possível consultar cervejas. Tente novamente mais tarde."
        static let noDataError = "Não há cervejas para exibir."
    }
    
}

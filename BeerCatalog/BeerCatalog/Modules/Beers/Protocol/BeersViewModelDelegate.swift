//
//  BeersViewModelDelegate.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

protocol BeersViewModelDelegate: class {
    
    func didGetData()
    
    func didGetError(withMessage message: String)
    
}

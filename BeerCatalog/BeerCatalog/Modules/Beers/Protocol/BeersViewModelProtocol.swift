//
//  BeersViewModelProtocol.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import Foundation
import Core

protocol BeersViewModelProtocol {
    
    var delegate: BeersViewModelDelegate? { get set }
    
    init(withProvider provider: NetworkingProviderProtocol)
    
    func getBeers() -> [BeerModel]?
    
    func fetch()
    
    func nextPage()
    
    func paginationRows() -> Int

}

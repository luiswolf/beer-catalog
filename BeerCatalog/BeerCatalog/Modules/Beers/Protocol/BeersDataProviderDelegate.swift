//
//  BeersDataProviderDelegate.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

protocol BeersDataProviderDelegate: class {
    
    func didSelect(item: BeerModel)
    
    func shouldGetMoreRows()
    
}

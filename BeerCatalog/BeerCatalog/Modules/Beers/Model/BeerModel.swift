//
//  BeerModel.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

struct BeerModel: Codable {
    
    private typealias Constant = BeersConstant
    
    let id: Int
    let name: String
    let abv: Double
    let tagline: String
    let ibu: Double?
    let description: String
    let imageUrl: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case abv
        case tagline
        case ibu
        case description
        case imageUrl = "image_url"
    }
    
}

// MARK: - Helper
extension BeerModel {
    
    var cleanImageUrl: String? {
        guard let imageUrl = imageUrl else { return nil }
        guard imageUrl != Constant.Image.defaultImageUrl else { return nil }
        return imageUrl
    }
    
}

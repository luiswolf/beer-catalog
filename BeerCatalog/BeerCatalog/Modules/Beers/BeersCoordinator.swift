//
//  BeersCoordinator.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit
import Core

class BeersCoordinator: CoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = BeerListTableViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - Actions
extension BeersCoordinator {
    
    func detail(ofBeer beer: BeerModel) {
        let vc = BeerDetailTableViewController(withBeer: beer)
        navigationController.pushViewController(vc, animated: true)
    }
    
}

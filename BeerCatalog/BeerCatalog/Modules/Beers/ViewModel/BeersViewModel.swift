//
//  BeersViewModel.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation
import Core

class BeersViewModel: NSObject, BeersViewModelProtocol {
    
    weak var delegate: BeersViewModelDelegate?
    
    private typealias Constant = BeersConstant
    
    private var beers: [BeerModel]?
    
    private var canLoadNextPage: Bool = false
    private let neworkingProvider: NetworkingProviderProtocol
    private lazy var request: BeersRequest = {
        let request = BeersRequest()
        return request
    }()
    
    required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider()) {
        self.neworkingProvider = provider
    }
    
    func getBeers() -> [BeerModel]? {
        return beers
    }
    
}

// MARK: - Networking
extension BeersViewModel {
    
    func fetch() {
        neworkingProvider.perform(request) { [weak self] (response: [BeerModel]?) in
            
            guard let response = response else {
                self?.canLoadNextPage = true
                self?.request.previousPage()
                self?.delegate?.didGetError(withMessage: Constant.Message.defaultError)
                return
            }

            guard !response.isEmpty else {
                self?.canLoadNextPage = false
                guard self?.beers == nil else {
                    self?.delegate?.didGetData()
                    return
                }
                self?.delegate?.didGetError(withMessage: Constant.Message.noDataError)
                return
            }

            self?.canLoadNextPage = true
            self?.beers = self?.beers ?? []
            self?.beers?.append(contentsOf: response)
            self?.delegate?.didGetData()
        }
    }
    
}

// MARK: - Pagination
extension BeersViewModel {
    
    func nextPage() {
        guard canLoadNextPage else { return }
        canLoadNextPage = false
        request.nextPage()
        fetch()
    }
    
    func paginationRows() -> Int {
        guard canLoadNextPage else { return 0 }
        return 1
    }
    
}

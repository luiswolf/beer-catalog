//
//  BeerTableViewCell.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit
import Core

class BeerTableViewCell: UITableViewCell, CellIdentifier {

    private typealias Constant = BeersConstant
    
    private(set) var isConfigured: Bool = false
    private let imageSize: CGFloat = 48
    
    private lazy var imagePreview: UIImageView = {
        let image = UIImage(named: Constant.Image.default)
        let view = UIImageView(image: image)
        view.tintColor = .darkOrange
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .title
        label.numberOfLines = 0
        label.textColor = .brown
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .darkOrange
        label.font = .subtitle
        return label
    }()
    
    private lazy var horizontalStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .center
        view.distribution = .fillProportionally
        view.spacing = 8.0
        view.addArrangedSubview(imagePreview)
        view.addArrangedSubview(verticalStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var verticalStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .leading
        view.distribution = .fill
        view.spacing = 2.0
        view.addArrangedSubview(titleLabel)
        view.addArrangedSubview(subtitleLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
}

// MARK: - Setup
extension BeerTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(horizontalStackView)
        let indicator = UIImageView(image: UIImage(named: Constant.Image.disclosureIndicator))
        indicator.tintColor = .darkOrange
        accessoryView = indicator
        let bgColorView = UIView()
        bgColorView.backgroundColor = .background
        selectedBackgroundView = bgColorView
        separatorInset = UIEdgeInsets(top: 0, left: imageSize * 1.25, bottom: 0, right: 0)
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let bottomConstraint = horizontalStackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        bottomConstraint.priority = .fittingSizeLevel
        let constraints = [
            imagePreview.widthAnchor.constraint(equalToConstant: imageSize),
            imagePreview.heightAnchor.constraint(equalTo: imagePreview.widthAnchor),
            horizontalStackView.topAnchor.constraint(equalTo: margin.topAnchor),
            bottomConstraint,
            horizontalStackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor, constant: -12.0),
            horizontalStackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func configure(withBeer beer: BeerModel) {
        imagePreview.image = UIImage(named: Constant.Image.default)
        if let imageUrl = beer.cleanImageUrl {
            imagePreview.download(from: imageUrl)
        }
        titleLabel.text = beer.name
        subtitleLabel.text = String(
            format: Constant.Label.abvWithValue,
            arguments: [Formatter.shared.percentValue(of: beer.abv)]
        )
        isConfigured = true
    }
    
}


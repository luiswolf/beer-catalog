//
//  HeaderTableViewCell.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit
import Core

class HeaderTableViewCell: UITableViewCell, CellIdentifier {
    
    private typealias Constant = BeersConstant
    
    private let imageSize: CGFloat = 128.0
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .center
        view.spacing = 16.0
        view.addArrangedSubview(imagePreview)
        view.addArrangedSubview(labelStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var labelStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .center
        view.spacing = 4.0
        view.addArrangedSubview(title)
        view.addArrangedSubview(subtitle)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imagePreview: UIImageView = {
        let image = UIImage(named: Constant.Image.default)
        let view = UIImageView(image: image)
        view.tintColor = .darkOrange
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.font = .title
        label.numberOfLines = 0
        label.textColor = .brown
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.font = .subtitle
        label.textColor = .darkOrange
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init(withBeer beer: BeerModel) {
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
        imagePreview.image = UIImage(named: Constant.Image.default)
        if let imageUrl = beer.cleanImageUrl {
            self.imagePreview.download(from: imageUrl)
        }
        title.text = beer.name
        subtitle.text = beer.tagline
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
}

// MARK: - Setup
extension HeaderTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(mainStackView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let constraints = [
            mainStackView.topAnchor.constraint(equalTo: margin.topAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            imagePreview.widthAnchor.constraint(equalToConstant: imageSize),
            imagePreview.heightAnchor.constraint(equalTo: imagePreview.widthAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}

//
//  BeersRequest.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation
import Core

struct BeersRequest: NetworkingRequestProtocol {
    
    private(set) var pageNumber: Int = 1
    private let pageSize: Int = 20
    
    var method: NetworkingMethod { .get }
    
    var url: String {
        BeersConstant.Endpoint.beers
            .replacingOccurrences(of: "{pageSize}", with: String(pageSize))
            .replacingOccurrences(of: "{pageNumber}", with: String(pageNumber))
    }
    
}

// MARK: - Helper
extension BeersRequest {
    
    mutating func nextPage() {
        pageNumber += 1
    }
    
    mutating func previousPage() {
        guard pageNumber > 1 else { return }
        pageNumber -= 1
    }
    
}

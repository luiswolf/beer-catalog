//
//  BeerListTableViewController.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit
import Core

class BeerListTableViewController: UITableViewController, LoaderProtocol, ErrorProtocol {

    private typealias Constant = BeersConstant
    
    weak var coordinator: BeersCoordinator?
    
    private var viewModel: BeersViewModelProtocol
    private lazy var dataProvider: BeerListDataProvider = {
        let provider = BeerListDataProvider(withTableView: tableView)
        provider.delegate = self
        return provider
    }()
    
    init(withViewModel viewModel: BeersViewModelProtocol = BeersViewModel()) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Lifecycle
extension BeerListTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Constant.Label.beers
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
        showLoader()
        viewModel.fetch()
    }
    
}

// MARK: - IssuesDataProviderDelegate
extension BeerListTableViewController: BeersDataProviderDelegate {
    
    func shouldGetMoreRows() {
        viewModel.nextPage()
    }
    
    func didSelect(item: BeerModel) {
        coordinator?.detail(ofBeer: item)
    }
    
}

// MARK: - IssuesViewModelDelegate
extension BeerListTableViewController: BeersViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        dataProvider.set(beers: viewModel.getBeers(), andPaginationRows: viewModel.paginationRows())
        tableView.reloadData()
    }
    
    func didGetError(withMessage message: String) {
        hideLoader()
        showError(withMessage: message)
    }
    
}

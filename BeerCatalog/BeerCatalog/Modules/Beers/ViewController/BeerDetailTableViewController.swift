//
//  BeerDetailTableViewController.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit

class BeerDetailTableViewController: UITableViewController {

    private typealias Constant = BeersConstant
    
    private let beer: BeerModel
    
    private lazy var dataProvider: BeerDetailDataProvider = {
        return BeerDetailDataProvider(withBeer: beer)
    }()
    
    init(withBeer beer: BeerModel) {
        self.beer = beer
        super.init(style: .grouped)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Lifecycle
extension BeerDetailTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = beer.name
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
    }
    
}

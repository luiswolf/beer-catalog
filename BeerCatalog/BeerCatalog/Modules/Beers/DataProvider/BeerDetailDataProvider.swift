//
//  BeerDetailDataProvider.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit
import Core

class BeerDetailDataProvider: NSObject {
    
    private typealias Constant = BeersConstant
    private typealias BasicCell = BasicTableViewCell
    private typealias RightDetailCell = RightDetailTableViewCell
    private typealias HeaderCell = HeaderTableViewCell
    
    private let beer: BeerModel
    private var rows = [UITableViewCell]()
    
    init(withBeer beer: BeerModel) {
        self.beer = beer
        super.init()
        self.configure()
    }
    
    private func configure() {
        rows.append(HeaderCell(withBeer: beer))
        rows.append(RightDetailCell(
            withTitle: Constant.Label.abv,
            andDetail: String(
                format: Constant.Label.abvWithValue,
                arguments: [Formatter.shared.percentValue(of: beer.abv)]
            )
        ))
        if let ibu = beer.ibu {
            rows.append(RightDetailCell(
                withTitle: Constant.Label.ibu,
                andDetail: String(
                    format: Constant.Label.ibuWithValue,
                    arguments: [Formatter.shared.decimalValue(of: ibu)]
                )
            ))
        }
        rows.append(BasicCell(withText: beer.description))
    }
    
}

// MARK: - UITableViewDelegate
extension BeerDetailDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
}

// MARK: - UITableViewDataSource
extension BeerDetailDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rows[indexPath.row]
        cell.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        return cell
    }
    
}

//
//  BeersDataProvider.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit
import Core

class BeerListDataProvider: NSObject {
    
    private typealias BeerCell = BeerTableViewCell
    private typealias LoaderCell = LoaderTableViewCell
    
    weak var delegate: BeersDataProviderDelegate?
    weak var tableView: UITableView?
    
    private var beers: [BeerModel]?
    private var paginationRows: Int = 0
    
    func set(beers: [BeerModel]?, andPaginationRows paginationRows: Int) {
        self.beers = beers
        self.paginationRows = paginationRows
    }
    
    private func register() {
        tableView?.register(BeerCell.self, forCellReuseIdentifier: BeerCell.identifier)
        tableView?.register(LoaderCell.self, forCellReuseIdentifier: LoaderCell.identifier)
    }
    
    init(withTableView tableView: UITableView?) {
        super.init()
        self.tableView = tableView
        self.register()
    }
    
}

// MARK: - UITableViewDelegate
extension BeerListDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let beers = beers else { return }
        let beer = beers[indexPath.row]
        delegate?.didSelect(item: beer)
    }
    
}

// MARK: - UITableViewDataSource
extension BeerListDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let beers = beers else { return 0 }
        return beers.count + paginationRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let beers = beers else { return UITableViewCell() }
        guard indexPath.row == beers.count else {
            let beer = beers[indexPath.row]
            return cell(forBeer: beer, atIndexPath: indexPath)
        }
        delegate?.shouldGetMoreRows()
        return cell(forLoaderAtIndexPath: indexPath)
    }
    
}

// MARK: - Helper
extension BeerListDataProvider {
    
    private func dequeue<T: CellIdentifier>(cellOfType type: T.Type, atIndexPath indexPath: IndexPath) -> T {
        return tableView?.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
    
    private func cell(forBeer beer: BeerModel,  atIndexPath indexPath: IndexPath) -> BeerCell {
        let cell = dequeue(cellOfType: BeerCell.self, atIndexPath: indexPath)
        cell.configure(withBeer: beer)
        return cell
    }

    private func cell(forLoaderAtIndexPath indexPath: IndexPath) -> LoaderCell {
        return dequeue(cellOfType: LoaderCell.self, atIndexPath: indexPath)
    }
    
}

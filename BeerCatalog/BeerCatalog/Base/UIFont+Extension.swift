//
//  UIFont+Extension.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import UIKit

extension UIFont {
    
    static let title = UIFont(name: "Thonburi-Bold", size: 16.0) ?? .boldSystemFont(ofSize: 16.0)
    static let subtitle = UIFont(name: "Thonburi", size: 13.0) ?? .systemFont(ofSize: 13.0)
    static let text = UIFont(name: "Thonburi", size: 16.0) ?? .systemFont(ofSize: 16.0)
    
}


//
//  UIImageView+Extension.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 13/03/21.
//

import UIKit
import Core

extension UIImageView {
    
    func download(from imageUrl: String) {
        let helper = ImageHelper(withProvider: NetworkingProvider())
        helper.get(imageWithPath: imageUrl) { (image) in
            self.image = image
        }
    }
    
}

//
//  AppDelegate.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit
import Core

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: CoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // appearance
        UINavigationBar.appearance().backgroundColor = .orange
        UINavigationBar.appearance().tintColor = .orange
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkOrange]
        UITableView.appearance().separatorColor = UIColor.lightOrange.withAlphaComponent(0.8)
        UITableView.appearance().backgroundColor = UIColor.background
        UIActivityIndicatorView.appearance().tintColor = .darkOrange
        UIActivityIndicatorView.appearance().color = .darkOrange
        BasicTableViewCell.appearance().customTextColor = .brown
        BasicTableViewCell.appearance().customFont = .text
        RightDetailTableViewCell.appearance().customTextColor = .brown
        RightDetailTableViewCell.appearance().customFont = .text
        
        let nc = UINavigationController()
        coordinator = BeersCoordinator(navigationController: nc)
        coordinator?.start()

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = nc
        window.makeKeyAndVisible()
        window.tintColor = .black
        
        self.window = window
        return true
    }
    
}

//
//  UIColor+Extension.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

extension UIColor {
    
    static let black = UIColor(named: "Black") ?? .black
    static let brown = UIColor(named: "Brown") ?? .brown
    static let yellow = UIColor(named: "Yellow") ?? .yellow
    static let lightOrange = UIColor(named: "LightOrange") ?? .orange
    static let orange = UIColor(named: "Orange") ?? .orange
    static let darkOrange = UIColor(named: "DarkOrange") ?? .orange
    static let background = UIColor(named: "Background") ?? .white
    
}

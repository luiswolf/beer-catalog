//
//  AppNetworkingProvider.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation
import Core
import Alamofire

class NetworkingProvider {}

extension NetworkingProvider: NetworkingProviderProtocol {
    
    func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
        
        AF.request(
            request.url,
            method: request.method.alamofireMethod,
            encoding: URLEncoding.default
        ).responseDecodable(
            of: T.self
        ){ response in
            DispatchQueue.main.async {
                callback(response.value)
            }
        }
        
    }
}

extension NetworkingProvider: NetworkingDownloadProviderProtocol {
    
    func download(withRequest request: NetworkingDownloadRequestProtocol, returningTo callback: @escaping DataCallback) {
        
        AF.download(request.url)
            .validate(contentType: request.contentType)
            .responseData { response in
            guard case let Result.success(data) = response.result else {
                DispatchQueue.main.async {
                    callback(nil)
                }
                return
            }
            DispatchQueue.main.async {
                callback(data)
            }
        }
        
    }
    
}

//
//  NetworkingMethod.swift
//  BeerCatalog
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation
import Alamofire
import Core

extension NetworkingMethod {
    
    var alamofireMethod: HTTPMethod {
        switch self {
            case .get: return .get
            case .post: return .post
            case .put: return .put
        }
    }
    
}

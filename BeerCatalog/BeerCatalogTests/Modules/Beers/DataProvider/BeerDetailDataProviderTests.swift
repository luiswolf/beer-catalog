//
//  BeerDetailDataProviderTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import BeerCatalog
@testable import Core

class BeerDetailDataProviderTests: XCTestCase {

    var dataProvider: BeerDetailDataProvider!
    var tableView: UITableView!
    
    override func setUp() {
        super.setUp()
        dataProvider = BeerDetailDataProvider(withBeer: BeersTestHelper.beer)
        tableView = UITableView()
        tableView.dataSource = dataProvider
        tableView.delegate = dataProvider
    }
    
    override func tearDown() {
        super.tearDown()
        dataProvider = nil
        tableView = nil
    }
    
    func test_whenIbuIsNotDefined_numberOfRowsAndKindsAreOk() {
        configure(withBeer: BeersTestHelper.beerWithNoIbu)
        
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 3)
        
        let firstCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))

        XCTAssertTrue(firstCell is HeaderTableViewCell)
        
        let secondCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        
        XCTAssertTrue(secondCell is RightDetailTableViewCell)
        
        let thirdCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 2, section: 0))
        
        XCTAssertTrue(thirdCell is BasicTableViewCell)
    }
    
    func test_whenIbuIsDefined_numberOfRowsAndKindsAreOk() {
        configure(withBeer: BeersTestHelper.beer)
        
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 4)
        
        let firstCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))

        XCTAssertTrue(firstCell is HeaderTableViewCell)
        
        let secondCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        
        XCTAssertTrue(secondCell is RightDetailTableViewCell)
        
        let thirdCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 2, section: 0))
        
        XCTAssertTrue(thirdCell is RightDetailTableViewCell)
        
        let fourthCell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 3, section: 0))
        
        XCTAssertTrue(fourthCell is BasicTableViewCell)
    }
    
    func test_heightForRow_isAutomaticDimension() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: 0)), UITableView.automaticDimension)
    }
    
    func test_heightForHeaderInSection_isEight() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForHeaderInSection: 0), 8.0)
    }
    
    func test_titleOfSectionHeader_isBlankSpace() {
        XCTAssertEqual(dataProvider.tableView(tableView, titleForHeaderInSection: 0), " ")
    }

}

extension BeerDetailDataProviderTests {
    
    func configure(withBeer beer: BeerModel) {
        dataProvider = BeerDetailDataProvider(withBeer: beer)
        tableView = UITableView()
        tableView.dataSource = dataProvider
        tableView.delegate = dataProvider
    }
    
}

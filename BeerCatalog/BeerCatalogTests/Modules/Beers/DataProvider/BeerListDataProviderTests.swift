//
//  BeerListDataProviderTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import BeerCatalog
@testable import Core

class BeerListDataProviderTests: XCTestCase {

    var dataProvider: BeerListDataProvider!
    var tableView: UITableView!
    var selectionDelegate: BeersDataProviderDelegateMock!
    
    override func setUp() {
        super.setUp()
        
        tableView = UITableView()
        selectionDelegate = BeersDataProviderDelegateMock()
        dataProvider = BeerListDataProvider(withTableView: tableView)
        dataProvider.delegate = selectionDelegate
    }
    
    override func tearDown() {
        super.tearDown()
        
        tableView = nil
        selectionDelegate = nil
        dataProvider = nil
    }
    
    func test_numberOfRows_isZeroAtStart() {
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 0)
    }
    
    func test_numberOfRows_isBeerCountWhenPaginationIsZero() {
        dataProvider.set(beers: [BeersTestHelper.beer, BeersTestHelper.beer], andPaginationRows: 0)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    func test_numberOfRows_isBeerCountWhenPaginationIsNotZero() {
        dataProvider.set(beers: [BeersTestHelper.beer, BeersTestHelper.beer], andPaginationRows: 1)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 3)
    }
    
    func test_titleOfSectionHeader_isBlankSpace() {
        XCTAssertEqual(dataProvider.tableView(tableView, titleForHeaderInSection: 0), " ")
    }
    
    func test_heightForRow_isAutomaticDimension() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: 0)), UITableView.automaticDimension)
    }
    
    func test_heightForHeaderInSection_isEight() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForHeaderInSection: 0), 8.0)
    }
    
    func test_cell_isNotSelectedAtStart() {
        dataProvider.set(beers: [BeersTestHelper.beer, BeersTestHelper.beer], andPaginationRows: 1)
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isNotSelectedOnActionIfTheresNoBeersSet() {
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isSelectedOnAction() {
        dataProvider.set(beers: [BeersTestHelper.beer, BeersTestHelper.beer], andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, true)
    }
    
    func test_selectionIssue_hasRightTitle() {
        let items = [BeersTestHelper.beer, BeersTestHelper.beer]
        dataProvider.set(beers: items, andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.selectedItem?.name, items.first?.name)
    }
    
    func test_defaultCelatasLoaded_whenTheresNoDta() {
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertFalse(cell is BeerTableViewCell)
        XCTAssertFalse(cell is LoaderTableViewCell)
    }
    
    func test_beerCell_isLoaded() {
        dataProvider.set(beers: [BeersTestHelper.beer], andPaginationRows: 0)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is BeerTableViewCell)
    }
    
    func test_loaderCell_isLoaded() {
        dataProvider.set(beers: [BeersTestHelper.beer], andPaginationRows: 1)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertTrue(cell is LoaderTableViewCell)
    }
    
    func test_paginationMethod_isNotCalled() {
        dataProvider.set(beers: [BeersTestHelper.beer], andPaginationRows: 0)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, false)
    }
    
    func test_paginationMethod_isCalled() {
        dataProvider.set(beers: [BeersTestHelper.beer], andPaginationRows: 1)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, true)
    }

}

extension BeerListDataProviderTests {

    class BeersDataProviderDelegateMock: BeersDataProviderDelegate {
        
        var didSelectCell: Bool = false
        var shouldGetMoreRowsValue: Bool = false
        var selectedItem: BeerModel?
        
        func didSelect(item: BeerModel) {
            didSelectCell = true
            selectedItem = item
        }
        
        func shouldGetMoreRows() {
            shouldGetMoreRowsValue = true
        }
    }
    
}

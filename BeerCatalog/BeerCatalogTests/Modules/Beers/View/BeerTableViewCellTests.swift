//
//  BeerTableViewCellTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import XCTest
@testable import BeerCatalog

class BeerTableViewCellTests: XCTestCase {

    func test_initWithCoder_isNil() {
        let cell = BeerTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_init_isCreated() {
        let cell = BeerTableViewCell(style: .default, reuseIdentifier: "cell")
        
        XCTAssertNotNil(cell)
        XCTAssertNotNil(cell.contentView)
    }
    
    func test_cell_isConfigured() {
        let cell = BeerTableViewCell(style: .default, reuseIdentifier: "cell")
        
        XCTAssertFalse(cell.isConfigured)
        cell.configure(withBeer: BeersTestHelper.beer)
        XCTAssertTrue(cell.isConfigured)
    }

}

//
//  HeaderTableViewCellTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 13/03/21.
//

import XCTest
@testable import BeerCatalog

class HeaderTableViewCellTests: XCTestCase {

    func test_initWithCoder_isNil() {
        let cell = HeaderTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_init_isCreated() {
        let cell = HeaderTableViewCell(withBeer: BeersTestHelper.beer)
        
        XCTAssertNotNil(cell)
        XCTAssertNotNil(cell.contentView)
    }
    
}

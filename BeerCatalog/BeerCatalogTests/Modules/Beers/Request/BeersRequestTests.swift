//
//  BeersRequestTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import XCTest
@testable import BeerCatalog

class BeersRequestTests: XCTestCase {

    var sut: BeersRequest!
    
    override func setUp() {
        super.setUp()
        
        sut = BeersRequest()
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
    }
    
    func test_url_isConsideringDefaultParameters() {
        let url = String(format: BeersTestHelper.apiUrl, arguments: [20, 1])
        XCTAssertEqual(sut.url, url)
    }
    
    func test_method_isGet() {
        XCTAssertEqual(sut.method, .get)
    }
    
    func test_pageNumber_startsInOne() {
        XCTAssertEqual(sut.pageNumber, 1)
    }
    
    func test_url_isRightAfterIncreasingPagination() {
        sut.nextPage()
        sut.nextPage()
        let url = String(format: BeersTestHelper.apiUrl, arguments: [20, 3])
        XCTAssertEqual(sut.url, url)
    }
    
    func test_pageNumber_isRightAfterIncreasingPagination() {
        sut.nextPage()
        sut.nextPage()
        XCTAssertEqual(sut.pageNumber, 3)
    }
    
    func test_pageNumber_isDefaultAfterOperations() {
        sut.nextPage() // 2
        sut.nextPage() // 3
        sut.nextPage() // 4
        sut.previousPage() // 3
        sut.previousPage() // 2
        XCTAssertEqual(sut.pageNumber, 2)
    }
    
    func test_pageNumber_isDefaultAfterExcededPreviousPagesOperations() {
        sut.previousPage()
        sut.previousPage()
        
        XCTAssertEqual(sut.pageNumber, 1)
    }
    
}

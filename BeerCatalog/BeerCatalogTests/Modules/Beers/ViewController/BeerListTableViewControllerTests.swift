//
//  BeerListTableViewControllerTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
import Core
@testable import BeerCatalog

class BeerListTableViewControllerTests: XCTestCase {

    var coordinator: BeersCoordinator!
    var sut: BeerListTableViewController!
    var viewModel: BeersViewModelMock!
    
    override func setUp() {
        super.setUp()
        coordinator = BeersCoordinator(navigationController: UINavigationController())
        viewModel = BeersViewModelMock()
        sut = BeerListTableViewController(withViewModel: viewModel)
        sut.coordinator = coordinator
    }
    
    override func tearDown() {
        super.tearDown()
        viewModel = nil
        sut = nil
    }
    
    func test_InitWithCoder_isNil() {
        let vc = BeerListTableViewController(coder: NSCoder())
        XCTAssertNil(vc)
    }
    
    func test_title_isSetAfterViewDidLoad() {
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.title, BeersConstant.Label.beers)
    }
    
    func test_tableView_hasBeerListDataProvider() {
        sut.loadViewIfNeeded()
        XCTAssertTrue(sut.tableView.delegate is BeerListDataProvider)
        XCTAssertTrue(sut.tableView.dataSource is BeerListDataProvider)
    }
    
    func test_fetchingData_shouldReturnError() {
        viewModel.returning = nil
        sut.loadViewIfNeeded()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingData_shouldReturnErrorWithNoData() {
        viewModel.returning = []
        sut.loadViewIfNeeded()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingData_shouldReturnSuccess() {
        viewModel.returning = [BeersTestHelper.beer, BeersTestHelper.beerWithNoIbu]
        sut.loadViewIfNeeded()
        
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getBeers()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertTrue(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_fetchingDataForPagination_shouldReturnError() {
        viewModel.returning = [BeersTestHelper.beer, BeersTestHelper.beerWithNoIbu]
        sut.loadViewIfNeeded()
        
        viewModel.returning = nil
        sut.shouldGetMoreRows()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingDataForPagination_shouldReturnSuccessWithNoData() {
        viewModel.returning = [BeersTestHelper.beer, BeersTestHelper.beerWithNoIbu]
        sut.loadViewIfNeeded()
        
        viewModel.returning = []
        sut.shouldGetMoreRows()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getBeers()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertFalse(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_fetchingDataForPagination_shouldReturnSuccess() {
        viewModel.returning = [BeersTestHelper.beer, BeersTestHelper.beerWithNoIbu]
        sut.loadViewIfNeeded()
        
        viewModel.returning = [BeersTestHelper.beerWithNoIbu]
        sut.shouldGetMoreRows()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getBeers()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertTrue(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_beerSelection_shouldNavigateToDetail() {
        sut.loadViewIfNeeded()
        sut.didSelect(item: BeersTestHelper.beer)
        let vc = coordinator.navigationController.topViewController
        XCTAssertTrue(vc is BeerDetailTableViewController)
        vc?.loadViewIfNeeded()
        XCTAssertEqual(vc?.title, BeersTestHelper.beer.name)
    }
        
}

extension BeerListTableViewControllerTests {
    class BeersViewModelMock: BeersViewModelProtocol {
        
        var page: Int = 0
        var canLoadNextPage: Bool = false
        
        weak var delegate: BeersViewModelDelegate?
        
        var beers: [BeerModel]?
        var returning: [BeerModel]?
        
        required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider()) {
            
        }
        
        func getBeers() -> [BeerModel]? {
            return beers
        }
        
        func fetch() {
            guard let returning = returning else {
                canLoadNextPage = true
                delegate?.didGetError(withMessage: "Teste")
                return
            }
            guard !returning.isEmpty else {
                canLoadNextPage = false
                guard beers == nil else {
                    delegate?.didGetData()
                    return
                }
                delegate?.didGetError(withMessage: "Teste")
                return
            }
            canLoadNextPage = true
            beers = beers ?? []
            beers?.append(contentsOf: returning)
            delegate?.didGetData()
        }
        
        func nextPage() {
            page += 1
            fetch()
        }
        
        func paginationRows() -> Int {
            guard canLoadNextPage else { return 0 }
            return 1
        }
        
    }
}


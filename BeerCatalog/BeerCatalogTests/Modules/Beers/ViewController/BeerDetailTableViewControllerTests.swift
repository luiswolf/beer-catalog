//
//  BeerDetailTableViewControllerTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 13/03/21.
//

import XCTest
import Core
@testable import BeerCatalog

class BeerDetailTableViewControllerTests: XCTestCase {
    
    var sut: BeerDetailTableViewController!
    var nc: UINavigationController!
    let beer = BeersTestHelper.beer
    
    override func setUp() {
        super.setUp()
        sut = BeerDetailTableViewController(withBeer: beer)
        nc = UINavigationController(rootViewController: sut)
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
        nc = nil
    }

    func test_InitWithCoder_isNil() {
        let vc = BeerDetailTableViewController(coder: NSCoder())
        XCTAssertNil(vc)
    }
    
    func test_title_isSetAfterViewDidLoad() {
        sut.loadViewIfNeeded()
        XCTAssertEqual(sut.title, beer.name)
    }
    
}

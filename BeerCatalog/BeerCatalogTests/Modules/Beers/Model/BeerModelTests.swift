//
//  BeerModelTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import XCTest
@testable import BeerCatalog

class BeerModelTests: XCTestCase {
    
    func test_emptyImageUrl_hasNoCleanImageUrl() {
        let sut = BeersTestHelper.beerWithNoImageUrl
        XCTAssertNil(sut.cleanImageUrl)
    }
    
    func test_defaultImageUrl_hasNoCleanImageUrl() {
        let sut = BeersTestHelper.beerWithDefaultImageUrl
        XCTAssertNil(sut.cleanImageUrl)
    }
    
    func test_imageUrl_hasCleanImageUrl() {
        let sut = BeersTestHelper.beer
        let cleanImageUrl = sut.cleanImageUrl
        XCTAssertEqual(sut.imageUrl, cleanImageUrl)
    }
    
}

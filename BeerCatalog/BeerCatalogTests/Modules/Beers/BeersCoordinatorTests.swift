//
//  BeersCoordinatorTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import XCTest
@testable import BeerCatalog

class BeersCoordinatorTests: XCTestCase {

    var sut: BeersCoordinator!
    var nc: UINavigationController!
    
    override func setUp() {
        super.setUp()
        nc = UINavigationController()
        sut = BeersCoordinator(navigationController: nc)
    }
    
    override func tearDown() {
        super.tearDown()
        nc = nil
        sut = nil
    }
    
    func test_beforeStart_shouldHaveNoViewControllers() {
        XCTAssertEqual(nc.viewControllers.count, 0)
    }
    
    func test_afterStart_beerListTableViewControllerIsTopViewController() {
        sut.start()
        XCTAssertTrue(nc.topViewController is BeerListTableViewController)
    }
    
    func test_onDetail_beerDetailTableViewControllerIsTopViewController() {
        
        sut.detail(ofBeer: BeersTestHelper.beer)
        XCTAssertTrue(nc.topViewController is BeerDetailTableViewController)
    }
    
}

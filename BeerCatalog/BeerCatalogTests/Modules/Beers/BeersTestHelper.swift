//
//  BeersTestHelper.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import Foundation
@testable import BeerCatalog

struct BeersTestHelper {
    
    static let apiUrl = BeersConstant.Endpoint.beers
        .replacingOccurrences(of: "{pageSize}", with: String("%d"))
        .replacingOccurrences(of: "{pageNumber}", with: String("%d"))
    
    static let beer = BeerModel(
        id: 1,
        name: "Test",
        abv: 4.5,
        tagline: "Tagline test",
        ibu: 9,
        description: "Test description",
        imageUrl: "https://images.punkapi.com/v2/4.png"
    )
    
    static let beerWithNoIbu = BeerModel(
        id: 1,
        name: "Test",
        abv: 4.5,
        tagline: "Tagline test",
        ibu: nil,
        description: "Test description",
        imageUrl: "https://images.punkapi.com/v2/4.png"
    )
    
    static let beerWithNoImageUrl = BeerModel(
        id: 1,
        name: "Test",
        abv: 4.5,
        tagline: "Tagline test",
        ibu: 9,
        description: "Test description",
        imageUrl: nil
    )
    
    static let beerWithDefaultImageUrl = BeerModel(
        id: 1,
        name: "Test",
        abv: 4.5,
        tagline: "Tagline test",
        ibu: 9,
        description: "Test description",
        imageUrl: "https://images.punkapi.com/v2/keg.png"
    )
    
}

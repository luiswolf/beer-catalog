//
//  BeersViewModelTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 13/03/21.
//

import XCTest
import Core
@testable import BeerCatalog

class BeersViewModelTests: XCTestCase {

    let beer = BeersTestHelper.beer
    
    func test_whenObjectIsNil_defaultErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = BeersViewModel(withProvider: provider)
        
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, BeersConstant.Message.defaultError)
    }
    
    func test_whenObjectIsEmpty_noDataErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: [BeerModel]())
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 0)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, BeersConstant.Message.noDataError)
    }
    
    func test_whenObjectIsSet_nextRequestGotNoData() {
        let provider = MockNetworkingProvider(withModel: [beer])
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        provider.empty()
        sut.nextPage()
        
        XCTAssertEqual(sut.paginationRows(), 0)
        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.getBeers()?.count, 1)
    }
    
    func test_whenObjectIsSet_dataIsLoaded() {
        let provider = MockNetworkingProvider(withModel: [beer])
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        
        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.getBeers()?.count, 1)
    }
    
    func test_whenTheresError_canLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 2)
    }
        
    func test_whenObjectIsEmpty_canNotLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: [BeerModel]())
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 1)
    }

    func test_whenObjectIsSet_canLoadNextPage() {
        let provider = MockNetworkingProvider(withModel: [beer])
        let sut = BeersViewModel(withProvider: provider)
        let delegate = BeersViewModelTestsDelegate()
        sut.delegate = delegate
        sut.fetch()
        sut.nextPage()
        XCTAssertEqual(provider.performCount, 2)
    }
    
}

// MARK: - Helper
extension BeersViewModelTests {
    
    class BeersViewModelTestsDelegate: BeersViewModelDelegate {

        var gotData: Bool = false
        var error: String?
        
        func didGetData() {
            gotData = true
        }
        
        func didGetError(withMessage message: String) {
            error = message
        }
    }
        
    class MockNetworkingProvider: NetworkingProviderProtocol {
        
        var performCount: Int = 0
        var model: [BeerModel]?
        
        init(withModel model: [BeerModel]?) {
            self.model = model
        }
        
        func empty() {
            model = []
        }
        
        func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
            performCount += 1
            callback(model as? T)
        }
        
    }

}

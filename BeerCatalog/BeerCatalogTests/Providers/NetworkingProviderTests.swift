//
//  NetworkingProviderTests.swift
//  BeerCatalogTests
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import BeerCatalog
@testable import Core

class NetworkingProviderTests: XCTestCase {

    var sut: NetworkingProvider!
    
    override func setUp() {
        super.setUp()
        sut = NetworkingProvider()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_downloadFromInvalidUrl_shouldReturnNil() {
        let e = expectation(description: "Download request")
        let request = NetworkingImageDownloadRequest(url: "https://images.punkapi.com/v2/keg.pn")
        sut.download(withRequest: request){ (data) in
            XCTAssertNil(data)
            e.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_downloadFromValidUrl_shouldReturnData() {
        let e = expectation(description: "Download request")
        let request = NetworkingImageDownloadRequest(url: "https://images.punkapi.com/v2/keg.png")
        sut.download(withRequest: request){ (data) in
            XCTAssertNotNil(data)
            e.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
}

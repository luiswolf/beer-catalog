//
//  CoordinatorProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public protocol CoordinatorProtocol {
    
    var childCoordinators: [CoordinatorProtocol] { get set }
    
    var navigationController: UINavigationController { get set }

    func start()
    
}

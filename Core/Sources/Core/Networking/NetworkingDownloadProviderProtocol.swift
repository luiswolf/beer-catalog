//
//  NetworkingDownloadProviderProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import Foundation

public protocol NetworkingDownloadProviderProtocol {
    
    func download(withRequest request: NetworkingDownloadRequestProtocol, returningTo callback: @escaping DataCallback)
    
}

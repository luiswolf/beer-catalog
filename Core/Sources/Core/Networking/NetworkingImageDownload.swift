//
//  NetworkingImageDownloadRequest.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import Foundation

public struct NetworkingImageDownloadRequest: NetworkingDownloadRequestProtocol {
    
    public var url: String
    
    public var contentType: [String] {
        ["image/*"]
    }
    
}

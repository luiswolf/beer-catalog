//
//  NetworkingProviderProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

public typealias CodableCallback<T: Codable> = (T?) -> Void
public typealias DataCallback = (Data?) -> Void

public protocol NetworkingProviderProtocol {
    
    func perform<T: Codable>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>)
    
}

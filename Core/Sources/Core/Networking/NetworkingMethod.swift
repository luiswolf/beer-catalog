//
//  NetworkingMethod.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

public enum NetworkingMethod {
    
    case get
    case post
    case put
    
}

//
//  NetworkingRequestProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

public protocol NetworkingRequestProtocol {
    
    var method: NetworkingMethod { get }
    
    var url: String { get }
    
}

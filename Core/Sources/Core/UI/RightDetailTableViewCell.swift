//
//  RightDetailTableViewCell.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit

public class RightDetailTableViewCell: UITableViewCell, CellIdentifier {
    
    @objc public dynamic var customFont: UIFont?
    @objc public dynamic var customTextColor: UIColor?
    
    public convenience init(withTitle title: String, andDetail detail: String) {
        self.init()
        textLabel?.text = title
        textLabel?.numberOfLines = 0
        detailTextLabel?.text = detail
        detailTextLabel?.numberOfLines = 0
        selectionStyle = .none
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    public override func didMoveToWindow() {
        super.didMoveToWindow()
        textLabel?.font = customFont
        textLabel?.textColor = customTextColor
        detailTextLabel?.font = customFont
        detailTextLabel?.textColor = customTextColor
    }
    
}

//
//  LoaderTableViewCell.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public class LoaderTableViewCell: UITableViewCell, CellIdentifier {

    private lazy var indicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.startAnimating()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        indicatorView.startAnimating()
    }

}

// MARK: - Setup
extension LoaderTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(indicatorView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        
        let constraints = [
            indicatorView.topAnchor.constraint(equalTo: margin.topAnchor),
            indicatorView.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            indicatorView.centerXAnchor.constraint(equalTo: margin.centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}


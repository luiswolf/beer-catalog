//
//  LoaderProtocol.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public protocol LoaderProtocol: UIViewController {
    
    func showLoader()
    
    func hideLoader()
    
}

// MARK: - Default
extension LoaderProtocol {
    
    public func showLoader() {
        hideLoader()
    
        let container = ContainerView<LoaderView>()
        container.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container)
        
        let margin = view.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: margin.topAnchor),
            container.bottomAnchor.constraint(equalTo: margin.bottomAnchor),
            container.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            container.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ])
    }
    
    public func hideLoader() {
        view.subviews.first(where: { $0 is ContainerView<LoaderView>})?.removeFromSuperview()
    }
    
}


//
//  File.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 12/03/21.
//

import UIKit

public protocol CellIdentifier: UITableViewCell {
    
    static var identifier: String { get }
    
}

extension CellIdentifier {

    public static var identifier: String { String(describing: Self.self) }

}

//
//  ImageHelper.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import UIKit

public final class ImageHelper {
    
    private let provider: NetworkingDownloadProviderProtocol
    
    public init(withProvider provider: NetworkingDownloadProviderProtocol) {
        self.provider = provider
    }
    
    public func get(imageWithPath path: String, andReturnTo callback: @escaping (UIImage?)->Void) {
        guard let image = CacheHelper.shared.getImage(forKey: path) else {
            let request = NetworkingImageDownloadRequest(url: path)
            provider.download(withRequest: request) { (data) in
                if let data = data, let image = UIImage(data: data) {
                    CacheHelper.shared.storeImage(image, forKey: path)
                    callback(image)
                } else {
                    callback(nil)
                }
            }
            return
        }
        callback(image)
    }
}


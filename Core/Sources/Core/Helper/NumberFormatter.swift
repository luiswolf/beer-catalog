//
//  File.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 11/03/21.
//

import Foundation

public class Formatter {
    
    private let formatter = NumberFormatter()
    
    public static let shared = Formatter()
    
    private init() {}
    
    public func set(locale: Locale) {
        formatter.locale = locale
    }
    
    public func decimalValue(of: Double) -> String {
        formatter.numberStyle = .decimal
        formatter.minimumIntegerDigits = 1
        formatter.maximumIntegerDigits = 3
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: of))!
    }
    
    public func percentValue(of: Double) -> String {
        formatter.numberStyle = .percent
        formatter.minimumIntegerDigits = 1
        formatter.maximumIntegerDigits = 3
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: of / 100))!
    }
    
}



//
//  NetworkingImageDownloadRequestTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class NetworkingImageDownloadRequestTests: XCTestCase {
    
    func test_contentType_isImageType() {
        let sut = NetworkingImageDownloadRequest(url: "http://")
        XCTAssertEqual(sut.contentType, ["image/*"])
    }
    
}


//
//  ImageHelperTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class ImageHelperTests: XCTestCase {
    
    var provider: MockNetworkingProvider!
    var sut: ImageHelper!
    let path = "https://test.com/image"
    
    override func setUp() {
        super.setUp()
        CacheHelper.shared.removeImage(forKey: path)
        provider = MockNetworkingProvider()
        sut = ImageHelper(withProvider: provider)
    }
    
    override func tearDown() {
        super.tearDown()
        
        provider = nil
        sut = nil
    }
    
    func test_getImage_shouldDownload_Return_andStorageInCache() {
        provider.returning = provider.mockImage
        sut.get(imageWithPath: path) { (image) in
            XCTAssertNotNil(image)
            XCTAssertNotNil(CacheHelper.shared.getImage(forKey: self.path))
        }
    }
    
    func test_getImage_shouldReturnNil() {
        provider.returning = provider.nilImage
        sut.get(imageWithPath: path) { (image) in
            XCTAssertNil(image)
        }
    }
    
    func test_getImage_shouldReturnImageFromCache() {
        let imageStored = UIImage(named: "error", in: Bundle.module, compatibleWith: nil)!
        CacheHelper.shared.storeImage(imageStored, forKey: path)
        
        sut.get(imageWithPath: path) { (image) in
            XCTAssertEqual(image, imageStored)
        }
    }
    

}

extension ImageHelperTests {
    
    class MockNetworkingProvider: NetworkingDownloadProviderProtocol {
        
        let mockImage = UIImage(named: "error", in: Bundle.module, compatibleWith: nil)?.pngData()
        let nilImage: Data? = nil
        
        var returning: Data?
        
        func download(withRequest request: NetworkingDownloadRequestProtocol, returningTo callback: @escaping DataCallback) {
            callback(returning)
        }
    }
    
}

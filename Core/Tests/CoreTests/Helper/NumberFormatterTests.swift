//
//  NumberFormatterTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class NumberFormatterTests: XCTestCase {
    
    let formatter = Formatter.shared
    
    override func setUp() {
        super.setUp()
        
        formatter.set(locale: Locale(identifier: "pt_BR"))
    }
    
    func test_decimalValue_returnsFormatedString() {
        // GIVEN
        let rondedValue = 2.0
        let oneDecimalsValue = 2.3
        let twoDecimalsValue = 2.33
        let threeDecimalsValue = 2.336
        
        // WHEN
        let formattedRondedValue = Formatter.shared.decimalValue(of: rondedValue)
        let formattedOneDecimalsValue = Formatter.shared.decimalValue(of: oneDecimalsValue)
        let formattedTwoDecimalsValue = Formatter.shared.decimalValue(of: twoDecimalsValue)
        let formattedThreeDecimalsValue = Formatter.shared.decimalValue(of: threeDecimalsValue)
        
        // THEN
        XCTAssertEqual(formattedRondedValue, "2")
        XCTAssertEqual(formattedOneDecimalsValue, "2,3")
        XCTAssertEqual(formattedTwoDecimalsValue, "2,33")
        XCTAssertEqual(formattedThreeDecimalsValue, "2,34")
    }
    
    func test_percentValue_returnsFormatedString() {
        // GIVEN
        let rondedValue = 2.0
        let oneDecimalsValue = 2.3
        let twoDecimalsValue = 2.33
        let threeDecimalsValue = 2.336
        
        // WHEN
        let formattedRondedValue = Formatter.shared.percentValue(of: rondedValue)
        let formattedOneDecimalsValue = Formatter.shared.percentValue(of: oneDecimalsValue)
        let formattedTwoDecimalsValue = Formatter.shared.percentValue(of: twoDecimalsValue)
        let formattedThreeDecimalsValue = Formatter.shared.percentValue(of: threeDecimalsValue)
        
        // THEN
        XCTAssertEqual(formattedRondedValue, "2%")
        XCTAssertEqual(formattedOneDecimalsValue, "2,3%")
        XCTAssertEqual(formattedTwoDecimalsValue, "2,33%")
        XCTAssertEqual(formattedThreeDecimalsValue, "2,34%")
    }
    
}

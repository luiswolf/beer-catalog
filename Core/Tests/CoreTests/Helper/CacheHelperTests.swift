//
//  CacheHelperTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class CacheHelperTests: XCTestCase {
    
    let cache = CacheHelper.shared
    let url = "https://image.com/url"
    
    override func setUp() {
        super.setUp()
        
        cache.removeImage(forKey: url)
    }
    
    func test_storeImage_shouldSaveInCache() {
        // GIVEN
        let image: UIImage = UIImage(named: "error", in: Bundle.module, compatibleWith: nil)!
        
        // WHEN
        cache.storeImage(image, forKey: url)
        
        // THEN
        XCTAssertEqual(image, cache.getImage(forKey: url))
    }
    
    func test_storeImage_shouldNotSaveInCache_becauseItsAlreadySaved() {
        // GIVEN
        let image1: UIImage = UIImage(named: "error", in: Bundle.module, compatibleWith: nil)!
        cache.storeImage(image1, forKey: url)
        // WHEN
        let image2: UIImage = UIImage(named: "image", in: Bundle.module, compatibleWith: nil)!
        cache.storeImage(image2, forKey: url)

        // THEN
        XCTAssertNotEqual(image2, cache.getImage(forKey: url))
    }
    
    func test_storedImage_removedWithSuccess() {
        // WHEN
        cache.removeImage(forKey: url)
        
        // THEN
        XCTAssertNil(cache.getImage(forKey: url))
    }
    
}

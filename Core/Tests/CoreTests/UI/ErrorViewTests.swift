//
//  ErrorViewTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class ErrorViewTests: XCTestCase {
    
    func test_initWithCoder_isNil() {
        let view = ErrorView(coder: NSCoder())
        XCTAssertNil(view)
    }
    
    func test_message_isShown() {
        let view = ErrorView(frame: .zero)
        view.message = "Test"
        
        let stackView = view.subviews.first(
            where: { $0 is UIStackView }
        ) as? UIStackView
        XCTAssertEqual(stackView?.arrangedSubviews.count, 2)
        let label = stackView?.arrangedSubviews.first(
            where: { $0 is UILabel }
        ) as? UILabel
        XCTAssertEqual(label?.text, "Test")
    }

}

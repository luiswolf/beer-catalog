//
//  LoaderTableViewCellTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class LoaderTableViewCellTests: XCTestCase {
    
    func test_initWithCoder_isNil() {
        let cell = LoaderTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_initWithDefault_isOk() {
        let cell = LoaderTableViewCell(style: .value1, reuseIdentifier: "cell")
        let view = cell.contentView.subviews.first { $0 is UIActivityIndicatorView }
        XCTAssertNotNil(view)
    }
    
    func test_prepareForReuse_keepsIndicatorAnimating() {
        let cell = LoaderTableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.prepareForReuse()
        let view = cell.contentView.subviews.first(where:  { $0 is UIActivityIndicatorView }) as? UIActivityIndicatorView
        
        XCTAssertTrue(view!.isAnimating)
    }

}

//
//  RightDetailTableViewCellTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class RightDetailTableViewCellTests: XCTestCase {
    
    func test_initWithCoder_isNil() {
        let cell = RightDetailTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_initWithConvenience_isOk() {
        let cell = RightDetailTableViewCell(withTitle: "Title", andDetail: "Detail")
        
        XCTAssertEqual(cell.textLabel?.text, "Title")
        XCTAssertEqual(cell.detailTextLabel?.text, "Detail")
    }
    
    func test_didMoveToWindow_setCustomAttributes() {
        let font: UIFont = .systemFont(ofSize: 15)
        let color: UIColor = .green
        
        let cell = RightDetailTableViewCell(withTitle: "Title", andDetail: "Detail")
        cell.customFont = font
        cell.customTextColor = color
        cell.didMoveToWindow()
        
        XCTAssertEqual(cell.textLabel?.font, font)
        XCTAssertEqual(cell.detailTextLabel?.font, font)
        XCTAssertEqual(cell.textLabel?.textColor, color)
        XCTAssertEqual(cell.detailTextLabel?.textColor, color)
    }
    
}


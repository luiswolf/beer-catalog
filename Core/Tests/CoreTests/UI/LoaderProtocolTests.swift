//
//  LoaderProtocolTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class LoaderProtocolTests: XCTestCase {
    
    class TestViewController: UIViewController, LoaderProtocol {}
    
    func test_loader_isShown() {
        let vc = TestViewController()
        vc.loadViewIfNeeded()
        vc.showLoader()
        let view = vc.view.subviews.first(where: {$0 is ContainerView<LoaderView>})
        XCTAssertNotNil(view)
    }
    
    func test_loader_isHidden() {
        let vc = TestViewController()
        vc.loadViewIfNeeded()
        vc.showLoader()
        let view = vc.view.subviews.first(where: {$0 is ContainerView<LoaderView>})
        vc.hideLoader()
        XCTAssertFalse(vc.view.subviews.contains(view!))
    }
    
}



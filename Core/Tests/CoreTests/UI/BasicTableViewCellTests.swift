//
//  BasicTableViewCellTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class BasicTableViewCellTests: XCTestCase {
    
    func test_initWithCoder_isNil() {
        let cell = BasicTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_initWithConvenience_isOk() {
        let cell = BasicTableViewCell(withText: "Text")
        
        XCTAssertEqual(cell.textLabel?.text, "Text")
    }
    
    func test_didMoveToWindow_setCustomAttributes() {
        let font: UIFont = .systemFont(ofSize: 15)
        let color: UIColor = .green
        
        let cell = BasicTableViewCell(withText: "Text")
        cell.customFont = font
        cell.customTextColor = color
        cell.didMoveToWindow()
        
        XCTAssertEqual(cell.textLabel?.font, font)
        XCTAssertEqual(cell.textLabel?.textColor, color)
    }
    
}


//
//  ErrorProtocolTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class ErrorProtocolTests: XCTestCase {
    
    class TestViewController: UIViewController, ErrorProtocol {}
    
    func test_error_isShown() {
        let vc = TestViewController()
        vc.loadViewIfNeeded()
        vc.showError(withMessage: "Teste")
        let view = vc.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(view)
    }
    
    func test_error_isHidden() {
        let vc = TestViewController()
        vc.loadViewIfNeeded()
        vc.showError(withMessage: "Teste")
        let view = vc.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        vc.hideError()
        XCTAssertFalse(vc.view.subviews.contains(view!))
    }
    
}



//
//  CellIdentifierTests.swift
//  
//
//  Created by Luis Emilio Dias Wolf on 14/03/21.
//

import XCTest
@testable import Core

final class CellIdentifierTests: XCTestCase {
    
    class TestTableViewCell: UITableViewCell, CellIdentifier {}
    
    func test_identifier_hasSameNameThatClass() {
        XCTAssertEqual(TestTableViewCell.identifier, "TestTableViewCell")
    }
    
}
